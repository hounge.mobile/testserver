<?php 
	class admin_model extends CI_Model {

		function __construct() {
	        parent::__construct();
	    }

		public function save($id, $url) {
			$data = array(
               'url' => $url,
               'date' => time()
            );

			$this->db->where('id', $id);
			$this->db->update('videos', $data); 
			return true;
		}	

		public function savenew($id, $url) {
			$data = array(
               'url' => $url,
               'date' => time()
            );

			$this->db->where('id', $id);
			$this->db->insert('videos', $data); 
			return true;
		}	

		public function delete($id) {
			$this->db->delete('videos', array('id' => $id));
			return true;
		}

		public function getVideos() {
			return $this->db->get('videos')->result_array();
		}
	}
?>