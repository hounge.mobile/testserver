<?php 
	class login_model extends CI_Model {

		function __construct() {
	        parent::__construct();
	    }

		public function login($username, $password) {
			$this->db->select('id');
			$this->db->from('users');
			$this->db->where('username', $username);
			$this->db->where('password', $password);
			$user = $this->db->get()->result_array();
			return $user;
		}	
	}
?>