<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Admin Panel</title>
		<!-- load twitter bootstrap CSS from CDN -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<!-- load font awesome from CDN -->
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- load open sans font from Google Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<!-- load main CSS file using asset helper -->
		<?php echo css_asset('admin.css'); ?>
		<!-- load jquery from CDN -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<!-- load twitter bootstrap javascript from CDN -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
	  		<div class="container">
	    		<a class="navbar-brand" href="#">Admin Panel</a>
	    		<a class="logout" href="javascript:void(0);">Logout</a>
	  		</div>
		</nav>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissible" role="alert" id="message">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <span class="message"></span>
					</div>
				</div>
				<?php 
					// start the counter for the video numbers (not the same as ID's)
					// it's just mean to number them starting at #1
					$i = 1; 
				?>
				<?php foreach ($videos as $video): ?>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Video #<?=$i;?><span class="delete" rel="<?=$video['id'];?>"><i class="fa fa-remove"></i></span></div>
					  		<div class="panel-body">
							    <form method="post" action="" class="form-horizontal">
									<div class="form-group">
										<label class="sr-only" for="url<?=$video['id'];?>">URL</label>
										<div class="col-md-12">
											<div class="input-group">
			      								<div class="input-group-addon">URL</div>
												<input type="text" class="form-control" id="url<?=$video['id'];?>" placeholder="Enter url" value="<?=$video['url'];?>">
											</div>
										</div>
									</div>
									<?php 
										// increment the video counter
										$i++; 
									?>
									<button type="button" class="btn btn-info save" rel="<?=$video['id'];?>">Save</button>
							    </form>
						  	</div>
						</div>
					</div>
				<?php endforeach; ?>
				<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Add New</div>
					  		<div class="panel-body">
							    <form method="post" action="" class="form-horizontal">
									<div class="form-group">
										<label class="sr-only" for="urlnew">URL</label>
										<div class="col-md-12">
											<div class="input-group">
			      								<div class="input-group-addon">URL</div>
												<input type="text" class="form-control" id="urlnew" placeholder="Enter url">
											</div>
										</div>
									</div>
									<button type="button" class="btn btn-warning" id="savenew">Save</button>
							    </form>
						  	</div>
						</div>
					</div>
			</div>
		</div>
		<script>
			$(document).ready(function() {
				$('.save').click(function() {
					var id = $(this).attr('rel');
					var url = $(this).parent().find('.form-control').val();
					$.ajax({
			            type: 'POST',
			            url: "<?php echo base_url(); ?>admin/save",
			            data: {id:id, url:url},
			            dataType: 'text',
			            success: function(response){
			              if (response == "ok") {
			              	$('#message .message').text('Video Saved!');
			              	$('#message').removeClass('alert-danger').addClass('alert-success');
			              } else {
			              	$('#message .message').text('Error Updating Video');
			              	$('#message').removeClass('alert-success').addClass('alert-danger');
			              }
			              $('#message').fadeIn();
			            }
			        });
				});
				$('#savenew').click(function() {
					var url = $('#urlnew').val();
					if (url == '') {
						$('#message .message').text('Please enter a valid URL');
			            $('#message').removeClass('alert-success').addClass('alert-danger').fadeIn();
					}
					$.ajax({
			            type: 'POST',
			            url: "<?php echo base_url(); ?>admin/savenew",
			            data: {url:url},
			            dataType: 'text',
			            success: function(response){
			              if (response) {
			              	location.reload();
			              } else {
			              	$('#message .message').text('Error Updating Video');
			              	$('#message').removeClass('alert-success').addClass('alert-danger').fadeIn();
			              }
			            }
			        });
				});
				$('.delete').click(function() {
					var id = $(this).attr('rel');
					if (id == '') {
						$('#message .message').text('Ooops - no video ID found!');
			            $('#message').removeClass('alert-success').addClass('alert-danger').fadeIn();
					}

					var r = confirm("Are you sure you want to delete this video?");
					if (r == true) {
					    $.ajax({
				            type: 'POST',
				            url: "<?php echo base_url(); ?>admin/delete",
				            data: {id:id},
				            dataType: 'text',
				            success: function(response){
				              if (response) {
				              	location.reload();
				              } else {
				              	$('#message .message').text('Error Deleting Video');
				              	$('#message').removeClass('alert-success').addClass('alert-danger').fadeIn();
				              }
				            }
				        });
					}
				});
				$('.logout').click(function() {
					window.location = 'login/logout';
				});
			});
		</script>
	</body>
</html>