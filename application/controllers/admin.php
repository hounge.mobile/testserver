<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('admin_model');
	}

	// this loads the main admin page with the videos
	public function index() {
		// check to see if they are logged in
		// if not, redirect them to the login page
		if (!$this->session->userdata('logged_in')) {
      		$this->session->sess_destroy();
			redirect('login');
      	} else {
			$this->load->view('file_view', array('error' => ' ' ));
		}
	}
	public function do_upload(){
		$config =  array(
			'upload_path'     => "./uploads/",
			'allowed_types'   => "mp4",
			'file_name' 	  => "video.mp4",
			'overwrite'       => TRUE,
			'max_size'        => "0",  // Can be set to particular file size
			'max_height'      => "0",
			'max_width'       => "0"
		);
		$this->load->library('upload', $config);
		if($this->upload->do_upload())
		{
			$data = array('upload_data' => $this->upload->data());
			$this->load->view('upload_success',$data);
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
	}



	public function save() {
		// get the ID and URL passed via POST
		$id = $this->input->post('id');
		$url = $this->input->post('url');
		// set status to false by default
		$status = false;
		// check that we have an ID and URL
		// if we do, try to save it
		if (!empty($id) && !empty($url)) {
			$saved = $this->admin_model->save($id, $url);
			// if it saved okay update the status to TRUE
			if ($saved) {
				$status = true;
			}
		} 
		// echo out the status so AJAX can see it
		// this becomes the response variable in the javascript
		echo $status;
	}

	public function savenew() {
		// get the URL passed over from POST
		$url = $this->input->post('url');
		// set status to false by default
		$status = false;
		// check that we have a URL
		// if we do, try to save it
		if (!empty($url)) {
			$saved = $this->admin_model->savenew($url);
			// if it saved okay update the status to TRUE
			if ($saved) {
				$status = true;
			}
		} 
		// echo out the status so AJAX can see it
		// this becomes the response variable in the javascript
		echo $status;
	}

	public function delete() {
		// get the ID passed over from POST
		$id = $this->input->post('id');
		// set status to false by default
		$status = false;
		// check that we have an ID
		// if we do, try to delete that record
		if (!empty($id)) {
			$deleted = $this->admin_model->delete($id);
			// if it deleted okay update the status to TRUE
			if ($deleted) {
				$status = true;
			}
		} 
		// echo out the status so AJAX can see it
		// this becomes the response variable in the javascript
		echo $status;
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */